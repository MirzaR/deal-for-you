package com.example.korisnik.d4y;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 11.04.2018..
 */

public class OdjecaFragment extends Fragment {
    private RecyclerView mProizvodiRecyclerView;
    private OdjecaAdapter mAdapter;

    public int indexKliknutog;
    TextView mNaslovTextView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_odjeca_list, container, false);


        mProizvodiRecyclerView = (RecyclerView) view
                .findViewById(R.id.proizvodi_recycler_view);
        mProizvodiRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI();
        return view;
    }


    private void updateUI() {
        //CrimeLab crimeLab = CrimeLab.get(getActivity());
        List<Proizvod> proizvodi = new MainActivity().proizvodi;
        mAdapter = new OdjecaAdapter(proizvodi);
        mProizvodiRecyclerView.setAdapter(mAdapter);
    }


    private class OdjecaHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private Proizvod mProizvod;

        public OdjecaHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater.inflate(R.layout.list_item_odjeca, parent, false));
            itemView.setOnClickListener(this);
            mNaslovTextView = (TextView) itemView.findViewById(R.id.Proizvod) ;
        }

        public void bind(Proizvod proizvod) {
            mProizvod = proizvod;
            String ispis = proizvod.getNaziv() + "\n" + "Cijena: " + proizvod.getCijena() + "\n";
            mNaslovTextView.setText(ispis);
        }

        @Override
        public void onClick(View view) {
            indexKliknutog = MainActivity.proizvodi.indexOf(mProizvod);

            Intent intent = ClickedActivity.newIntent(getActivity(), Integer.toString(indexKliknutog));
            startActivity(intent);
           /* Toast.makeText(getActivity(),
                    mProizvod.getNaziv() + " clicked!", Toast.LENGTH_SHORT)
                    .show();*/
        }
    }
    private class OdjecaAdapter extends RecyclerView.Adapter<OdjecaHolder> {
        private List<Proizvod> mProizvodi;
        public OdjecaAdapter(List<Proizvod> Proizvodi) {
            mProizvodi = Proizvodi;
        }
        @Override
        public OdjecaHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            return new OdjecaHolder(layoutInflater, parent);
        }
        @Override
        public void onBindViewHolder(OdjecaHolder holder, int position) {
            Proizvod proizvod = mProizvodi.get(position);
            holder.bind(proizvod);
        }
        @Override
        public int getItemCount(){
            return mProizvodi.size();
        }
    }

}
