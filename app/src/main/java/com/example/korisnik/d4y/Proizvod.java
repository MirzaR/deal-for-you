package com.example.korisnik.d4y;

/**
 * Created by User on 11.04.2018..
 */

public class Proizvod {
    private String naziv;
    private String kategorija;
    private String prodavnica;
    private String cijena;
    private String pocetnaCijena;
    private int snizenje;

    public Proizvod(){
        this.naziv="";
        this.cijena="";
        this.pocetnaCijena="";
        this.kategorija="";
        this.prodavnica="";
        this.snizenje=0;
    }


    public Proizvod(String naziv, String cijena, String pocetnaCijena, String kategorija, String prodavnica, int snizenje){
        this.naziv=naziv;
        this.cijena=cijena;
        this.kategorija=kategorija;
        this.prodavnica=prodavnica;
        this.pocetnaCijena = pocetnaCijena;
        this.snizenje = snizenje;
    }

    public int getSnizenje() {
        return snizenje;
    }

    public void setSnizenje(int snizenje) {
        this.snizenje = snizenje;
    }

    public String getPocetnaCijena() {
        return pocetnaCijena;
    }

    public void setPocetnaCijena(String pocetnaCijena) {
        this.pocetnaCijena = pocetnaCijena;
    }

    public String getCijena() {
        return cijena;
    }

    public void setCijena(String cijena) {
        this.cijena = cijena;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getKategorija() {
        return kategorija;
    }

    public void setKategorija(String kategorija) {
        this.kategorija = kategorija;
    }

    public String getProdavnica() {
        return prodavnica;
    }

    public void setProdavnica(String prodavnica) {
        this.prodavnica = prodavnica;
    }
}

