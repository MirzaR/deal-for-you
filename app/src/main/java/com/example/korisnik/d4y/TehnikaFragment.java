package com.example.korisnik.d4y;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by User on 11.04.2018..
 */

public class TehnikaFragment extends Fragment {
    private Proizvod mProizvod;
    private TextView mTekst;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tehnika, container, false);

        mTekst = (TextView) v.findViewById(R.id.tekstTehnika);

        return v;
    }
}
