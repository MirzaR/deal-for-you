package com.example.korisnik.d4y;

import android.app.ActionBar;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.korisnik.d4y.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button mHomeButton;
    Button mCategoryButton;
    Button mShopsButton;
    TextView mLarge;
    TextView mMedium;
    TextView mSmall;
    List<Proizvod> prikazivanje;
    public Proizvod mProizvod;
    public static final List<Proizvod> proizvodi = new ArrayList<>(100);

    private List<Proizvod> vratiNajvecaSnizenja(List<Proizvod> sviProizvodi){
        Proizvod proizvod1, proizvod2, proizvod3;
        List<Proizvod> rezultat;
        rezultat = new ArrayList<>();

        if(sviProizvodi.get(0).getSnizenje() > sviProizvodi.get(1).getSnizenje() && sviProizvodi.get(0).getSnizenje() > sviProizvodi.get(2).getSnizenje() ){
            proizvod1 = sviProizvodi.get(0);
            if(sviProizvodi.get(1).getSnizenje() > sviProizvodi.get(2).getSnizenje()){
                proizvod2 = sviProizvodi.get(1);
                proizvod3 = sviProizvodi.get(2);
            } else {
                proizvod2 = sviProizvodi.get(2);
                proizvod3 = sviProizvodi.get(1);
            }
        } else if (sviProizvodi.get(1).getSnizenje() > sviProizvodi.get(0).getSnizenje() && sviProizvodi.get(1).getSnizenje() > sviProizvodi.get(2).getSnizenje()){
            proizvod1 = sviProizvodi.get(1);
            if(sviProizvodi.get(0).getSnizenje() > sviProizvodi.get(2).getSnizenje()){
                proizvod2 = sviProizvodi.get(1);
                proizvod3 = sviProizvodi.get(2);
            } else {
                proizvod2 = sviProizvodi.get(2);
                proizvod3 = sviProizvodi.get(0);
            }
        } else {
            proizvod1 = sviProizvodi.get(2);
            if(sviProizvodi.get(0).getSnizenje() > sviProizvodi.get(1).getSnizenje()){
                proizvod2 = sviProizvodi.get(0);
                proizvod3 = sviProizvodi.get(1);
            } else {
                proizvod2 = sviProizvodi.get(1);
                proizvod3 = sviProizvodi.get(0);
            }
        }

        for(int i=3; i<sviProizvodi.size(); i++){
            if(sviProizvodi.get(i).getSnizenje() > proizvod1.getSnizenje()){
                Proizvod pomocni = proizvod1;
                proizvod1 = sviProizvodi.get(i);
                proizvod3 = proizvod2;
                proizvod2 = pomocni;
            }
            else if(sviProizvodi.get(i).getSnizenje() > proizvod2.getSnizenje()){
                proizvod3 = proizvod2;
                proizvod2 = sviProizvodi.get(i);
            }
            else if(sviProizvodi.get(i).getSnizenje() > proizvod3.getSnizenje()){
                proizvod3 = sviProizvodi.get(i);
            }
        }
        rezultat.add(proizvod1);
        rezultat.add(proizvod2);
        rezultat.add(proizvod3);


        return rezultat;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHomeButton = (Button) findViewById(R.id.HomeButton);

        mHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,
                        MainActivity.class);
                startActivity(myIntent);

            }
        });

        mCategoryButton = (Button) findViewById(R.id.CategoryButton);

        mCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,
                        CategoryActivity.class);
                startActivity(myIntent);

            }
        });

        mShopsButton = (Button) findViewById(R.id.ShopsButton);

        mShopsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this,
                        ShopsActivity.class);
                startActivity(myIntent);

            }
        });

        mLarge = (TextView) findViewById(R.id.najvecaAkcija);
       /*String ispis = prikazivanje.get(0).getNaziv() + "\n" + "Cijena: " + prikazivanje.get(0).getCijena() + "\n" + "Stara cijena" +
                prikazivanje.get(0).getPocetnaCijena() + "\n" + "Usteda: " + prikazivanje.get(0).getSnizenje() + "%";*/

        mMedium = (TextView) findViewById(R.id.srednjaAkcija);
        mSmall = (TextView) findViewById(R.id.najmanjaAkcija);



//SKIDANJE INFORMACIJA
        new Thread(new Runnable() {
            @Override
            public void run () {
                try {
                    Document konekcija = Jsoup.connect("http://www.boohoo.com/womens/sale").get();
                    Log.v("Konektovano", "str");

                    for(org.jsoup.nodes.Element proizvod : konekcija.select("div.product-tile")) {
                        Log.v("konektovano", "petlja");
                        String naslov = proizvod.select("a.name-link").text();
                        String cijena = proizvod.select("span.product-sales-price").text();
                        String pocetnaCijena = proizvod.select("span.product-standard-price").text();
                        Log.v("snizena cijena", cijena);
                        // String snizenje = proizvod.select("span.product-sales-price span.product-sales-price--percent").text();

                        //uzimanje samo broja iz cijena
                        String cijena1="";
                        String pocetnaCijena1="";

                        for (int i=0; i<cijena.length(); i++){
                            if(Character.isDigit(cijena.charAt(i))){
                                cijena1 += cijena.charAt(i);
                            }
                        }
                        for (int i=0; i<pocetnaCijena.length(); i++){
                            if(Character.isDigit(pocetnaCijena.charAt(i))){
                                pocetnaCijena1 += pocetnaCijena.charAt(i);
                            }
                        }

                        int snizenje = 100 - Integer.parseInt(cijena1)*100/Integer.parseInt(pocetnaCijena1);


                        Proizvod NoviProizvod = new Proizvod(naslov, cijena, pocetnaCijena, "odjeca", "Boohoo", snizenje);
                        proizvodi.add(NoviProizvod);
                        Log.v("Velicina", Integer.toString(proizvodi.size()));
                        if(proizvodi.size() == 50) break;

                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                    Log.v("Catch:", "Palo");
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.v("Pkrenuto", "hahe");
                        Log.v("Velicina run",  Integer.toString(proizvodi.size()));

                        prikazivanje = vratiNajvecaSnizenja(proizvodi);



                        String ispis1 = prikazivanje.get(0).getNaziv() + "\n" + "Cijena: " + prikazivanje.get(0).getCijena() + "\n" + "Stara cijena: " +
                                prikazivanje.get(0).getPocetnaCijena() + "\n" + "Usteda: " + prikazivanje.get(0).getSnizenje() + "%";
                        mLarge.setText(ispis1);

                        String ispis2 = prikazivanje.get(1).getNaziv() + "\n" + "Cijena: " + prikazivanje.get(1).getCijena() + "\n" + "Stara cijena: " +
                                prikazivanje.get(1).getPocetnaCijena() + "\n" + "Usteda: " + prikazivanje.get(1).getSnizenje() + "%";
                        mMedium.setText(ispis2);

                        String ispis3 = prikazivanje.get(2).getNaziv() + "\n" + "Cijena: " + prikazivanje.get(2).getCijena() + "\n" + "Stara cijena: " +
                                prikazivanje.get(2).getPocetnaCijena() + "\n" + "Usteda: " + prikazivanje.get(2).getSnizenje() + "%";
                        mSmall.setText(ispis3);
                    }
                });
            }
        }).start();

    }

}
