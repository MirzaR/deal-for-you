package com.example.korisnik.d4y;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;


public class CategoryActivity extends AppCompatActivity {

    private static final String EXTRA_IZBOR =
            "com.example.korisnik.d4y.izbor";


    Button mHomeButton;
    Button mCategoryButton;
    Button mShopsButton;
    Button mCategoryListButton;
    Spinner dynamicSpinner;
    List<String> items;
    ArrayAdapter<String> adapter;
    String item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);


        dynamicSpinner = (Spinner) findViewById(R.id.dynamic_spinner);

        items = new ArrayList<String>();
        items.add("Hrana");
        items.add("Odjeca");
        items.add("Obuca");
        items.add("Kozmetika");
        items.add("Tehnika");

        adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, items);

        dynamicSpinner.setAdapter(adapter);


        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                item = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        mHomeButton = (Button) findViewById(R.id.HomeButton);

        mHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(CategoryActivity.this,
                        MainActivity.class);
                startActivity(myIntent);

            }
        });

        mCategoryButton = (Button) findViewById(R.id.CategoryButton);

        mCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(CategoryActivity.this,
                        CategoryActivity.class);
                startActivity(myIntent);

            }
        });

        mShopsButton = (Button) findViewById(R.id.ShopsButton);

        mShopsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(CategoryActivity.this,
                        ShopsActivity.class);
                startActivity(myIntent);


                //startActivity(myIntent);


            }
        });

        mCategoryListButton = (Button) findViewById(R.id.CategoryListButton);

        mCategoryListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = CategoryList.newIntent(CategoryActivity.this, item);

                startActivity(myIntent);

            }
        });
    }
}
