package com.example.korisnik.d4y;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

public class ShopsActivity extends AppCompatActivity {

    Button mHomeButton;
    Button mCategoryButton;
    Button mShopsButton;
    Button mShopsListButton;
    Spinner dynamicSpinner2;
    String[] items2;
    String item;
    ArrayAdapter<String> adapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops);

        dynamicSpinner2 = (Spinner) findViewById(R.id.dynamic_spinner);

        items2 = new String[] { "Boohoo", "Domod", "Mercator", "Bingo", "DM" };

        adapter2 = new ArrayAdapter<String>(this,
                R.layout.spinner_item, items2);

        dynamicSpinner2.setAdapter(adapter2);

        dynamicSpinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                item = (String) parent.getItemAtPosition(position);
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });



        mHomeButton = (Button) findViewById(R.id.HomeButton);

        mHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ShopsActivity.this,
                        MainActivity.class);
                startActivity(myIntent);

            }
        });

        mCategoryButton = (Button) findViewById(R.id.CategoryButton);

        mCategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ShopsActivity.this,
                        CategoryActivity.class);
                startActivity(myIntent);

            }
        });

        mShopsButton = (Button) findViewById(R.id.ShopsButton);

        mShopsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ShopsActivity.this,
                        ShopsActivity.class);
                startActivity(myIntent);

            }
        });

        mShopsListButton = (Button) findViewById(R.id.ShopsListButton);

        mShopsListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = ShopsList.newIntent(ShopsActivity.this,
                        item);
                startActivity(myIntent);

            }
        });
    }

}