package com.example.korisnik.d4y;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ShopsList extends AppCompatActivity {

    private static final String EXTRA_SHOPS_IZBOR =
            "com.example.korisnik.d4y.izborShops";

    private String mIzborShops;
    TextView mTekst;
    ClothesListActivvity mCLA;

    public static Intent newIntent(Context packageContext, String izbor) {
        Intent intent = new Intent(packageContext, ShopsList.class);
        intent.putExtra(EXTRA_SHOPS_IZBOR, izbor);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shops_list);

        mIzborShops = getIntent().getStringExtra(EXTRA_SHOPS_IZBOR);
        mTekst = (TextView) findViewById(R.id.Tekst);

        if(mIzborShops.equals("Boohoo")){
            mCLA = new ClothesListActivvity();
            FragmentManager fm = getSupportFragmentManager();
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fragment = mCLA.createFragment();
                fm.beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit();
            }

        }
        else if(mIzborShops.equals("Domod")){

            mTekst.setText("Usli ste u prodavnicu: Domod \n Stranica u izradi, hvala na razumijevanju!");

        }
        else if(mIzborShops.equals("Mercator")){
            mTekst.setText("Usli ste u prodavnicu: Mercator \n Stranica u izradi, hvala na razumijevanju!");
        }
        else if(mIzborShops.equals("Bingo")){
            mTekst.setText("Usli ste u prodavnicu: Bingo \n Stranica u izradi, hvala na razumijevanju!");
        }
        else if(mIzborShops.equals("DM")){
            mTekst.setText("Usli ste u prodavnicu: DM \n Stranica u izradi, hvala na razumijevanju!");
        }


    }


}
