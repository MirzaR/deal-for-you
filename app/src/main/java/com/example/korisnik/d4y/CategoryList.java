package com.example.korisnik.d4y;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class CategoryList extends AppCompatActivity {

    private static final String EXTRA_IZBOR =
            "com.example.korisnik.d4y.izbor";

    private String mIzbor;
    ClothesListActivvity mCLA;

    public static Intent newIntent(Context packageContext, String izbor) {
        Intent intent = new Intent(packageContext, CategoryList.class);
        intent.putExtra(EXTRA_IZBOR, izbor);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_list);

        FragmentManager fm = getSupportFragmentManager();
        mIzbor = getIntent().getStringExtra(EXTRA_IZBOR);




        if(mIzbor.equals("Hrana")) {
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fragment = new HranaFragment();
                fm.beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit();
            }

        }
        else if(mIzbor.equals("Odjeca")){

            mCLA = new ClothesListActivvity();
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fragment = mCLA.createFragment();
                fm.beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit();
            }

        }
        else if(mIzbor.equals("Obuca")){
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fragment = new ObucaFragment();
                fm.beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit();
            }

        }
        else if(mIzbor.equals("Kozmetika")){
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fragment = new KozmetikaFragment();
                fm.beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit();
            }

        }
        else if(mIzbor.equals("Tehnika")){
            Fragment fragment = fm.findFragmentById(R.id.fragment_container);
            if (fragment == null) {
                fragment = new TehnikaFragment();
                fm.beginTransaction()
                        .add(R.id.fragment_container, fragment)
                        .commit();
            }

        }


    }


}