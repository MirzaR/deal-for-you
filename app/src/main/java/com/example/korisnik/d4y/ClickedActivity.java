package com.example.korisnik.d4y;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class ClickedActivity extends AppCompatActivity {

    TextView mPrikaz;
    int index;
    Intent intent;

    public static final String EXTRA_PRIKAZ =
            "com.example.korisnik.d4y.indexKliknutog";
    public static Intent newIntent(Context packageContext, String izbor) {
        Intent intent = new Intent(packageContext, ClickedActivity.class);
        intent.putExtra(EXTRA_PRIKAZ, izbor);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clicked);

        mPrikaz = (TextView) findViewById(R.id.prikaz);
        index = Integer.parseInt(getIntent().getStringExtra(EXTRA_PRIKAZ));
        Proizvod proizvod = MainActivity.proizvodi.get(index);
        String ispis = proizvod.getNaziv() + "\n" + "Cijena: " + proizvod.getCijena() + "\n" + "Stara cijena: " +
                proizvod.getPocetnaCijena() + "\n" + "Usteda: " + proizvod.getSnizenje() + "%";

        mPrikaz.setText(ispis);


        Log.v("Index", Integer.toString(index));
    }

}
